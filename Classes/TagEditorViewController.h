//
//  TagEditorViewController.h
//  ID3 Editor
//
//  Created by Readdle on 03.08.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MP3SelectListViewController.h"
#import "GetMP3TagsOperation.h"
@class MP3Tag;
@class MP3Info;

//TagEditorDelegate Protocol
@protocol TagEditorDelegate
- (void)findTagsDidTap;
- (void)getMp3InfoWithFilePath:(NSString *)filePath completionBlock:(void (^)(MP3Info*))completionBlock;
- (void)doneDidTapp:(MP3Tag*)mp3Tag filePath:(NSString *)filePath;
@end

//TagEditorDataSource Protocol
@protocol TagEditorDataSource
-(NSString*)mp3SelectPath;
@end

@interface TagEditorViewController : UIViewController

@property (nonatomic) BOOL isFindButtonActivityIndicatorVisible;
@property (nonatomic) BOOL isCoverImageViewActivityIndicatorVisible;
- (void)setMp3Info:(MP3Info*)mp3Info;

@property (weak, nonatomic) id <TagEditorDataSource> dataSource;
@property (weak, nonatomic) id <TagEditorDelegate> delegate;

@end
