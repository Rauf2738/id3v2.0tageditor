//
//  Fingerprint.h
//  TestChromaplast
//
//  Created by Readdle on 21.07.17.
//  Copyright © 2017 RGS. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MP3Tag;

@interface MP3FingerPrint : NSObject

@property double duration;
@property NSString* fingerprint;

@end

@interface FingerprintModule : NSObject

- (MP3FingerPrint*)generateFingerPrint:(NSURL*)songUrl;

@end
