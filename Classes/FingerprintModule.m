//
//  Fingerprint.m
//  TestChromaplast
//
//  Created by Readdle on 21.07.17.
//  Copyright © 2017 RGS. All rights reserved.
//

#import "FingerprintModule.h"
#import <AVFoundation/AVFoundation.h>
#import "MP3Tag.h"
#import "GetMP3TagsOperation.h"
#import <Chroma/chromaprint.h>

@implementation MP3FingerPrint
@end

@implementation FingerprintModule

- (MP3FingerPrint*)generateFingerPrint:(NSURL*)songUrl{
    int algo = INT32_C(CHROMAPRINT_ALGORITHM_TEST2);
    ChromaprintContext * chromaprintContext = chromaprint_new(algo);
    double duration = [self decodeAudio:songUrl withMaxLenght:120 forContext:chromaprintContext];
    char *fingerprint = NULL;
    if (chromaprint_get_fingerprint(chromaprintContext, &fingerprint) == 0) {
        NSLog(@"Error: could not get fingerprint");
    }
    NSString * fingerprintString = [NSString stringWithCString:fingerprint encoding:NSASCIIStringEncoding];
    chromaprint_dealloc(chromaprintContext);
    NSLog(@"%@", fingerprintString);
    NSLog(@"%f", duration);
    
    MP3FingerPrint *result = [[MP3FingerPrint alloc] init];
    result.duration = duration;
    result.fingerprint = fingerprintString;
    
    return result;
}


- (double)decodeAudio:(NSURL*)fromUrl withMaxLenght:(int)maxLength forContext:(ChromaprintContext*)context{
    double duration = 120;
    
    AVURLAsset* asset = [[AVURLAsset alloc] initWithURL: fromUrl options:nil];
    AVAssetReader* reader = [[AVAssetReader alloc] initWithAsset:asset error:nil];
    NSArray *audioTracks = [asset tracksWithMediaType:AVMediaTypeAudio];
    
    if(audioTracks == NULL){
        NSLog(@"Error: No audio tracks found");
    }
    
    NSDictionary *outputSettings = @{
                                     AVFormatIDKey: [NSNumber numberWithInt:kAudioFormatLinearPCM],
                                     AVLinearPCMIsBigEndianKey: [NSNumber numberWithInt:0],
                                     AVLinearPCMIsFloatKey: [NSNumber numberWithInt:0],
                                     AVLinearPCMBitDepthKey: [NSNumber numberWithInt:16],
                                     AVLinearPCMIsNonInterleaved: [NSNumber numberWithInt:0]
                                     };
    
    AVAssetTrack* audioTrack = audioTracks[0];
    
    AVAssetReaderTrackOutput *trackOutput = [[AVAssetReaderTrackOutput alloc] initWithTrack:audioTrack outputSettings:outputSettings];
    
    duration = CMTimeGetSeconds(audioTrack.timeRange.duration);
    
    
    UInt32 sampleRate = 0, channelCount = 0;
    NSArray* descriptions = audioTrack.formatDescriptions;
    CMAudioFormatDescriptionRef audioFormstDesc = (__bridge CMAudioFormatDescriptionRef)descriptions[0];
    const AudioStreamBasicDescription* audioStreamDesc = CMAudioFormatDescriptionGetStreamBasicDescription(audioFormstDesc);
    if(audioStreamDesc)
    {
        sampleRate = audioStreamDesc->mSampleRate;
        channelCount = audioStreamDesc->mChannelsPerFrame;
    }
    
    
    int rate = sampleRate;
    int channels = channelCount;
    
    [reader addOutput:trackOutput];
    [reader startReading];
    
    int totalBuf = 0;
    int remainingSamples = (int32_t)maxLength * channels * rate;
    
    chromaprint_start(context, rate, channels);
    
    while (reader.status == AVAssetReaderStatusReading){
        CMSampleBufferRef sampleBufferRef = [trackOutput copyNextSampleBuffer];
        if(sampleBufferRef != nil){
            CMBlockBufferRef blockBufferRef = CMSampleBufferGetDataBuffer(sampleBufferRef);
            if(blockBufferRef != nil){
                int bufferLength = (unsigned int)CMBlockBufferGetDataLength(blockBufferRef);
                totalBuf += bufferLength;
                NSMutableData *data = [NSMutableData dataWithLength:bufferLength];
                CMBlockBufferCopyDataBytes(blockBufferRef, 0, bufferLength, data.mutableBytes);
                
                int sampleCount = INT32_C(bufferLength>>1);
                int length = MIN(remainingSamples, sampleCount);
                chromaprint_feed(context, data.mutableBytes, length);
                
                CMSampleBufferInvalidate(sampleBufferRef);
                
                if (maxLength != 0) {
                    remainingSamples -= length;
                    if (remainingSamples <= 0) {
                        break;
                    }
                }
            }
        }
    }
    [reader cancelReading];
    chromaprint_finish(context);
    return duration;
}

@end
