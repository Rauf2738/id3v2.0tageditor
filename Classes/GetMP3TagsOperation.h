//
//  GetMP3TagsOperation.h
//  ID3 Editor
//
//  Created by Readdle on 25.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GetMP3CoverOperation.h"

@class MP3Tag;

@class GetMP3CoverOperation;

@protocol GetMP3TagsResultProtocol <NSObject>

- (NSError *)error;
- (BOOL)isCancelled;

- (NSArray <MP3Tag*> *) mp3Tags;


@end

@interface GetMP3TagsOperation : NSObject 
- (instancetype)initWithMP3FileAtPath:(NSString*)filePath;
- (void)startWithCompletionBlock:(void (^)(id <GetMP3TagsResultProtocol> result))completionBlock;
- (void)cancel;

@end
