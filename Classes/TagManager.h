//
//  TagManager.h
//  ID3 Editor
//
//  Created by Readdle on 29.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TagEditorViewController.h"

@class MP3Tag;

@interface TagManager : NSObject

@property (nonatomic, readonly) NSCache *cacheController;

//Singletone
@property (nonatomic, readonly, class) TagManager *sharedInstance;

- (instancetype)init NS_UNAVAILABLE;

- (MP3Tag *)readTagsForMP3FileAtPath:(NSString *)filePath;
- (void)setTags:(MP3Tag *)tags forMP3FileAtPath:(NSString *)filePath;
- (void)removeTags:(NSString*)filePath;


@end
