//
//  TagEditorViewController.m
//  ID3 Editor
//
//  Created by Readdle on 03.08.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "TagEditorViewController.h"
#import "TagEditorTableViewCell.h"
#import "TagManager.h"
#import "MP3Info.h"
#import "CoverWithMimeType.h"
#import "HeaderTagSelectionView.h"

#define kOFFSET_FOR_KEYBOARD 80.0

typedef enum : NSUInteger {
    Song=0,
    Artist=1,
    Album=2,
    Genre=3,
    Track=4,
    Year=5,
    Time=6,
    Kind=7,
    MSize=8,
    BitRate=9
} MP3TagObjects;

@interface TagEditorViewController () <UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate, TagEditorTableViewCellProtocol, UIPickerViewDataSource, UIPickerViewDelegate>

@property HeaderTagSelectionView *header;
@property UITableView *tagTableView;

@property NSArray <NSString*> * genresKind;

@property NSArray <NSArray <NSString*> *> *tagSections;
@property NSMutableDictionary <NSString *, NSString *> *tagValuesMap;

@property TagManager* tagManager;
@property (nonatomic) MP3Info* mp3Info;

@property UIBarButtonItem *findButton;
@property UIActivityIndicatorView *activityIndicatorBarForButton;
@property UIActivityIndicatorView *activityIndicatorForCoverImageView;
@property UIPickerView *genrePicker;

@property CGSize kbSize;

@property BOOL keyboardIsShowing;

@end

@implementation TagEditorViewController

-(instancetype)init{
    self = [super init];
    
    UITapGestureRecognizer *tapToHideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    [self.view addGestureRecognizer:tapToHideKeyboard];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(pressCancel:)];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pressDone:)];
    _findButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(pressFind:)];
    _activityIndicatorBarForButton = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    _activityIndicatorBarForButton.color = UIColor.blueColor;
    [_activityIndicatorBarForButton startAnimating];
    
    _genrePicker = [[UIPickerView alloc] init];
    _genrePicker.dataSource = self;
    _genrePicker.delegate = self;
    
    self.navigationItem.leftBarButtonItem = cancelButton;
    self.navigationItem.rightBarButtonItems = @[doneButton, _findButton];
    self.navigationItem.title = @"Info";
    
    _tagSections = @[[[NSArray alloc] initWithObjects:@"Song", @"Artist", @"Album" , @"Genre" , @"Track", @"Year",  nil], [[NSArray alloc] initWithObjects: @"Time" , @"Kind", @"Size", @"BitRate", nil]];
    _genresKind = [[NSArray alloc] initWithObjects:@"Rap", @"Rock", @"Pop", @"K-Pop", @"R&B", @"Country", @"Latin", @"EDM",  nil];
    
    _tagManager = [TagManager sharedInstance];
    _tagValuesMap = [NSMutableDictionary dictionary];
    
    return self;
}

-(void)setMp3Info:(MP3Info *)mp3Info{
    _mp3Info = mp3Info;
    [self reloadData];
}

- (void)loadView {
    [super loadView];
    self.view.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1.0];
    
    _header = [[HeaderTagSelectionView alloc] init];
    _header.translatesAutoresizingMaskIntoConstraints = NO;
    UITapGestureRecognizer *coverImageEditImageVewTapInside = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getImageFromPhotoLibrary:)];
    [_header.coverImageView addGestureRecognizer:coverImageEditImageVewTapInside];
    
    _tagTableView = [[UITableView alloc] init];
    _tagTableView.delegate = self;
    _tagTableView.dataSource = self;
    [_tagTableView registerClass:[TagEditorTableViewCell class] forCellReuseIdentifier:@"Cell"];
    _tagTableView.translatesAutoresizingMaskIntoConstraints = NO;
    
    _activityIndicatorForCoverImageView = [[UIActivityIndicatorView alloc] init];
    _activityIndicatorForCoverImageView.color = UIColor.darkGrayColor;
    _activityIndicatorForCoverImageView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_header];
    [self.view addSubview:_tagTableView];
    [self.view addSubview:_activityIndicatorForCoverImageView];
    
    //HeaderViewConstraints
    NSLayoutConstraint *headerViewTopConstraint =[NSLayoutConstraint
                                                  constraintWithItem:_header
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                  toItem:self.topLayoutGuide
                                                  attribute:NSLayoutAttributeBottom
                                                  multiplier:1.0f
                                                  constant:0.f];
    NSLayoutConstraint *headerViewLeftConstraint =[NSLayoutConstraint
                                                   constraintWithItem:_header
                                                   attribute:NSLayoutAttributeLeft
                                                   relatedBy:NSLayoutRelationEqual
                                                   toItem:self.view
                                                   attribute:NSLayoutAttributeLeft
                                                   multiplier:1.0f
                                                   constant:0.f];
    NSLayoutConstraint *headerViewRightConstraint =[NSLayoutConstraint
                                                    constraintWithItem:_header
                                                    attribute:NSLayoutAttributeRight
                                                    relatedBy:NSLayoutRelationEqual
                                                    toItem:self.view
                                                    attribute:NSLayoutAttributeRight
                                                    multiplier:1.0f
                                                    constant:0.f];
    NSLayoutConstraint *headerViewHeightConstraint =[NSLayoutConstraint
                                                     constraintWithItem:_header
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                     toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                     multiplier:1.0f
                                                     constant:80.f];
    
    
    //TagTableViewConstraints
    NSLayoutConstraint *tagTableViewTopConstraint =[NSLayoutConstraint
                                                    constraintWithItem:_tagTableView
                                                    attribute:NSLayoutAttributeTop
                                                    relatedBy:NSLayoutRelationEqual
                                                    toItem:_header
                                                    attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0f
                                                    constant:10.f];
    NSLayoutConstraint *tagTableViewLeftConstraint =[NSLayoutConstraint
                                                     constraintWithItem:_tagTableView
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                     toItem:self.view
                                                     attribute:NSLayoutAttributeLeft
                                                     multiplier:1.0f
                                                     constant:0.f];
    NSLayoutConstraint *tagTableViewRightConstraint =[NSLayoutConstraint
                                                      constraintWithItem:_tagTableView
                                                      attribute:NSLayoutAttributeRight
                                                      relatedBy:NSLayoutRelationEqual
                                                      toItem:self.view
                                                      attribute:NSLayoutAttributeRight
                                                      multiplier:1.0f
                                                      constant:0.f];
    
    //TagTableViewConstraints
    NSLayoutConstraint *activityIndicatorForCoverImageCenterXConstraint =[NSLayoutConstraint
                                                                          constraintWithItem:_activityIndicatorForCoverImageView
                                                                          attribute:NSLayoutAttributeCenterX
                                                                          relatedBy:NSLayoutRelationEqual
                                                                          toItem:[[_header subviews] firstObject]
                                                                          attribute:NSLayoutAttributeCenterX
                                                                          multiplier:1.0f
                                                                          constant:0.f];
    NSLayoutConstraint *activityIndicatorForCoverImageCenterYConstraint =[NSLayoutConstraint
                                                                          constraintWithItem:_activityIndicatorForCoverImageView
                                                                          attribute:NSLayoutAttributeCenterY
                                                                          relatedBy:NSLayoutRelationEqual
                                                                          toItem:[[_header subviews] firstObject]
                                                                          attribute:NSLayoutAttributeCenterY
                                                                          multiplier:1.0f
                                                                          constant:0.f];
    
    [self.view addConstraint:headerViewTopConstraint];
    [self.view addConstraint:headerViewRightConstraint];
    [self.view addConstraint:headerViewLeftConstraint];
    [self.view addConstraint:headerViewHeightConstraint];
    
    [self.view addConstraint:tagTableViewTopConstraint];
    [self.view addConstraint:tagTableViewLeftConstraint];
    [self.view addConstraint:tagTableViewRightConstraint];
    
    [self.view addConstraint:activityIndicatorForCoverImageCenterXConstraint];
    [self.view addConstraint:activityIndicatorForCoverImageCenterYConstraint];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
    [self getMp3TagsFromSelectedPath];
    
    NSLayoutConstraint *tagTableViewHeightConstraint =[NSLayoutConstraint
                                                       constraintWithItem:_tagTableView
                                                       attribute:NSLayoutAttributeHeight
                                                       relatedBy:NSLayoutRelationEqual
                                                       toItem:nil
                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                       multiplier:1.0f
                                                       constant:_tagTableView.contentSize.height];
    NSLayoutConstraint *tagTableViewBottomConstraint =[NSLayoutConstraint
                                                       constraintWithItem:_tagTableView
                                                       attribute:NSLayoutAttributeBottom
                                                       relatedBy:NSLayoutRelationEqual
                                                       toItem:self.view
                                                       attribute:NSLayoutAttributeBottom
                                                       multiplier:1.0f
                                                       constant:0.f];
    if([[UIScreen mainScreen] bounds].size.height > 568) //iPhone 5 screen size
        [self.view addConstraint:tagTableViewHeightConstraint];
    else
        [self.view addConstraint:tagTableViewBottomConstraint];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tagTableView flashScrollIndicators];
}

- (void)getMp3TagsFromSelectedPath{
    if(_mp3Info == nil){
        [_delegate getMp3InfoWithFilePath:[_dataSource mp3SelectPath] completionBlock:^(MP3Info *result){
            _mp3Info = result;
            [self reloadData];
        }];
    }
}

//ReloadData on Views
-(void)reloadData{
    [_header setMP3Tag:_mp3Info.mp3Tags];
    
    _tagValuesMap[@"Song"] = _mp3Info.mp3Tags.title;
    _tagValuesMap[@"Artist"] = _mp3Info.mp3Tags.artist;
    _tagValuesMap[@"Album"] = _mp3Info.mp3Tags.album;
    _tagValuesMap[@"Genre"] = _mp3Info.mp3Tags.genre;
    _tagValuesMap[@"Track"] = _mp3Info.mp3Tags.track;
    _tagValuesMap[@"Year"] = _mp3Info.mp3Tags.year;
    _tagValuesMap[@"Time"] = _mp3Info.time;
    _tagValuesMap[@"Kind"] = _mp3Info.kind;
    _tagValuesMap[@"Size"] = _mp3Info.size;
    _tagValuesMap[@"BitRate"] = _mp3Info.bitRate;
    
    [self.tagTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - tableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _tagSections.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_tagSections[section] count];
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section == 0)
        return @"";
    else
        return @" ";
}
- (NSString *)tagTitleForCellAtIndexPath:(NSIndexPath *)cellIndexPath
{
    return [_tagSections[cellIndexPath.section][cellIndexPath.row] stringByAppendingString:@":"];
}

- (NSString *)tagStringValueForCellAtIndexPath:(NSIndexPath *)cellIndexPath
{
    return _tagValuesMap[_tagSections[cellIndexPath.section][cellIndexPath.row]];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TagEditorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.delegate = self;
    cell.textField.text = [self tagStringValueForCellAtIndexPath:indexPath];
    cell.titleToTextfield.text = [self tagTitleForCellAtIndexPath:indexPath];
    if([cell.titleToTextfield.text  isEqual: @"Time:"] || [cell.titleToTextfield.text  isEqual: @"Size:"] || [cell.titleToTextfield.text  isEqual: @"Kind:"] || [cell.titleToTextfield.text  isEqual: @"BitRate:"]){
        cell.textField.userInteractionEnabled = NO;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        [[(TagEditorTableViewCell*)[_tagTableView cellForRowAtIndexPath:indexPath] textField] becomeFirstResponder];
    }
}

#pragma mark -

-(void)readAndSetTagsFromTextfield{
    if(_mp3Info.mp3Tags.title != _tagValuesMap[@"Song"]) {
        _mp3Info.mp3Tags.title = _tagValuesMap[@"Song"];
    }
    if(_mp3Info.mp3Tags.artist != _tagValuesMap[@"Artist"]) {
        _mp3Info.mp3Tags.artist = _tagValuesMap[@"Artist"];
    }
    if(_mp3Info.mp3Tags.album != _tagValuesMap[@"Album"]) {
        _mp3Info.mp3Tags.album = _tagValuesMap[@"Album"];
    }
    if(_mp3Info.mp3Tags.genre != _tagValuesMap[@"Genre"]) {
        _mp3Info.mp3Tags.genre = _tagValuesMap[@"Genre"];
    }
    if(_mp3Info.mp3Tags.track != _tagValuesMap[@"Track"]) {
        _mp3Info.mp3Tags.track = _tagValuesMap[@"Track"];
    }
    if(_mp3Info.mp3Tags.year != _tagValuesMap[@"Year"]) {
        _mp3Info.mp3Tags.year = _tagValuesMap[@"Year"];
    }
    if(_mp3Info.size != _tagValuesMap[@"Size"]) {
        _mp3Info.size = _tagValuesMap[@"Size"];
    }
    if(_mp3Info.kind != _tagValuesMap[@"Kind"]) {
        _mp3Info.kind = _tagValuesMap[@"Kind"];
    }
    if(_mp3Info.bitRate != _tagValuesMap[@"BitRate"]) {
        _mp3Info.bitRate = _tagValuesMap[@"BitRate"];
    }
    if(_mp3Info.time != _tagValuesMap[@"Time"]) {
        _mp3Info.time = _tagValuesMap[@"Time"];
    }
    if(_mp3Info.mp3Tags.coverWithMimeType.image != _header.coverImageView.image){
        _mp3Info.mp3Tags.coverWithMimeType.image = _header.coverImageView.image;
    }
}

//NavigationBar Button Actions
- (IBAction)pressDone:(id)sender{
    [self.view endEditing:YES];
    [self readAndSetTagsFromTextfield];
    [_delegate doneDidTapp:_mp3Info.mp3Tags filePath:[_dataSource mp3SelectPath]];
}
- (IBAction)pressFind:(id)sender{
    [_delegate findTagsDidTap];
}
- (IBAction)pressCancel:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)keyboardHide:(id)sender{
    if(_keyboardIsShowing == YES){
        [self.view endEditing:YES];
    }
}
//Get Image from Photo Library
-(void)getImageFromPhotoLibrary:(UIGestureRecognizer *)recognizer{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePickerController animated:TRUE completion:nil];
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    _header.coverImageView.image = info[UIImagePickerControllerOriginalImage];
    _mp3Info.mp3Tags.coverWithMimeType.image = _header.coverImageView.image;
    _mp3Info.mp3Tags.coverWithMimeType.mimeType = PNGMimeType;
    //    Set MimeType Of Image
    //        if([[[info[UIImagePickerControllerReferenceURL] lastPathComponent] substringFromIndex:6]  isEqual: @"JPG"]){
    //            _mp3Tag.coverWithMimeType.mimeType = JPGMimeType;
    //        }
    //        else if([[[info[UIImagePickerControllerReferenceURL] lastPathComponent] substringFromIndex:6]  isEqual: @"PNG"]){
    //            _mp3Tag.coverWithMimeType.mimeType = PNGMimeType;
    //        }
    //        else {
    //            _mp3Tag.coverWithMimeType.mimeType = UnknownMimeType;
    //        }
    [picker dismissViewControllerAnimated:true completion:nil];
}


-(void)tagEditingCellDidBeginEditing:(id)cell{
    NSIndexPath *cellIndexPath = [self.tagTableView indexPathForCell:cell];
    if(cellIndexPath.section == 0 && cellIndexPath.row == 3){
        [(TagEditorTableViewCell*)[_tagTableView cellForRowAtIndexPath:cellIndexPath] textField].inputView = _genrePicker;
    }
    if(cellIndexPath.section == 0 && cellIndexPath.row == 4){
        [[(TagEditorTableViewCell*)[_tagTableView cellForRowAtIndexPath:cellIndexPath] textField] setKeyboardType:UIKeyboardTypePhonePad];
    }
    if(cellIndexPath.section == 0 && cellIndexPath.row == 5){
        [[(TagEditorTableViewCell*)[_tagTableView cellForRowAtIndexPath:cellIndexPath] textField] setKeyboardType:UIKeyboardTypePhonePad];
    }
}


//Reload _tagValuesMap to new value
-(void)tagEditingCellDidChangeValue:(id)cell newValue:(NSString *)newValue
{
    NSIndexPath *cellIndexPath = [self.tagTableView indexPathForCell:cell];
    NSString *title = _tagSections[cellIndexPath.section][cellIndexPath.row];
    _tagValuesMap[title] = newValue;
}

//Activity Indicator BOOL
-(void)setIsFindButtonActivityIndicatorVisible:(BOOL)findButtonActivityIndicatorVisible{
    if(findButtonActivityIndicatorVisible == YES){
        _findButton.customView = _activityIndicatorBarForButton;
        _findButton.customView.frame = CGRectMake(10, 10, 40, 40);
    }
    else{
        _findButton.customView = nil;
    }
}

-(void)setIsCoverImageViewActivityIndicatorVisible:(BOOL)isCoverImageViewActivityIndicatorVisible{
    if(isCoverImageViewActivityIndicatorVisible == YES)
        [_activityIndicatorForCoverImageView startAnimating];
    else
        [_activityIndicatorForCoverImageView stopAnimating];
}



- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    _keyboardIsShowing = YES;
    NSDictionary* info = [aNotification userInfo];
    _kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    if(_kbSize.height <= 216){
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, _kbSize.height, 0.0);
        _tagTableView.contentInset = contentInsets;
        _tagTableView.scrollIndicatorInsets = contentInsets;
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    _keyboardIsShowing = NO;
    if(_kbSize.height <= 216){
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        _tagTableView.contentInset = contentInsets;
        _tagTableView.scrollIndicatorInsets = contentInsets;
    }
}

//PickerView
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return  1;
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 5;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [_genresKind objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
    [(TagEditorTableViewCell*)[_tagTableView cellForRowAtIndexPath:indexPath] textField].text = [_genresKind objectAtIndex:row];
}

@end
