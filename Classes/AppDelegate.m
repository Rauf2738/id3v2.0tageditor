//
//  AppDelegate.m
//  ID3 Editor
//
//  Created by Readdle on 6/26/17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "AppDelegate.h"
#import "DataSourceFromFilePath.h"
#import "AppModel.h"
#import "TagManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSArray *mp3PathArray = [[NSArray alloc] init];

    UIPasteboard *pasterBoard = [UIPasteboard generalPasteboard];
    if([pasterBoard.string rangeOfString:@".mp3"].location != NSNotFound){
        NSURL *mp3Url = [[NSURL alloc] initWithString:pasterBoard.string];
        NSData *mp3Data = [NSData dataWithContentsOfURL:mp3Url];

        NSArray *libraryPath = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *directoryPath = [libraryPath objectAtIndex:0];

        NSString *mp3FilePath = [NSString stringWithFormat:@"%@/1.mp3", directoryPath];
        [mp3Data writeToFile:mp3FilePath atomically:NO];
        mp3PathArray = [[NSArray alloc] initWithObjects:mp3FilePath, @"/Users/readdle/Downloads/Imagine Dragons_-_Believer.mp3", nil];
    }
    else{
        mp3PathArray = [[NSArray alloc] initWithObjects:@"/Users/readdle/Downloads/Imagine Dragons_-_Believer.mp3", nil];
    }
    NSMutableArray *mp3FilePaths = [[NSMutableArray alloc] init];
    mp3FilePaths = [self copyFileToBundle:mp3PathArray];

    DataSourceFromFilePath* dataSourceView = [[DataSourceFromFilePath alloc] init: mp3FilePaths];
    AppModel *appModel = [[AppModel alloc] init:dataSourceView];
    TagManager.sharedInstance.cacheController.totalCostLimit = 10;
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    [self.window setRootViewController:appModel.navigationController];
    
    return YES;
}


- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)option{
    NSURL *mp3Url = [[NSURL alloc] initWithString:[[url absoluteString] substringFromIndex:3]];
    NSData *mp3Data = [NSData dataWithContentsOfURL:mp3Url];

    NSArray *libraryPath = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *directoryPath = [libraryPath objectAtIndex:0];

    NSString *mp3FilePath = [NSString stringWithFormat:@"%@/1.mp3", directoryPath];
    [mp3Data writeToFile:mp3FilePath atomically:NO];

    NSArray *mp3PathArray = [[NSArray alloc] initWithObjects:mp3FilePath, nil];

    NSMutableArray *mp3FilePaths = [[NSMutableArray alloc] init];
    mp3FilePaths = [self copyFileToBundle:mp3PathArray];

    DataSourceFromFilePath* dataSourceView = [[DataSourceFromFilePath alloc] init: mp3FilePaths];
    AppModel *appModel = [[AppModel alloc] init:dataSourceView];
    TagManager.sharedInstance.cacheController.totalCostLimit = 10;

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    [self.window setRootViewController:appModel.navigationController];

    return YES;
}

-(NSMutableArray*)copyFileToBundle:(NSArray*)mp3FilePath{
    NSArray *mp3PathArray = mp3FilePath;

    NSMutableArray *mp3FilePaths = [NSMutableArray new];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *libraryPath = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *directoryPath = [libraryPath objectAtIndex:0];
    for (int i = 0; i<mp3PathArray.count; i++){
        NSString *mp3Name = [mp3PathArray[i] lastPathComponent];
        NSString *imagePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:mp3Name];
        NSString *mp3Path = [[NSString alloc] initWithString: directoryPath];
        NSString *mp3NameWithSlash = [NSString stringWithFormat:@"%@%@", @"/", mp3Name];
        mp3Path = [mp3Path stringByAppendingString: mp3NameWithSlash];
        [fileManager copyItemAtPath:imagePathFromApp toPath:mp3Path error:nil];

        [mp3FilePaths addObject:mp3Path];
    }
    return mp3FilePaths;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // if clipboard contains url
    // show alert to download and edit mp3
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
