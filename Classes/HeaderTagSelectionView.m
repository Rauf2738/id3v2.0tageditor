//
//  HeaderTagSelectionView.m
//  ID3 Editor
//
//  Created by Readdle on 19.08.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "HeaderTagSelectionView.h"
#import "MP3Info.h"
#import "CoverWithMimeType.h"

@interface HeaderTagSelectionView()

@property UIActivityIndicatorView *activityIndicatorForCoverImageView;;

@end

@implementation HeaderTagSelectionView

-(instancetype)init{
    self = [super init];
    if(self){
        self.userInteractionEnabled = YES;
        //Create Views
        _coverImageView = [[UIImageView alloc] init];
        _coverImageView.backgroundColor = UIColor.darkGrayColor;
        _coverImageView.userInteractionEnabled = YES;
        _coverImageView.translatesAutoresizingMaskIntoConstraints = NO;
        _coverImageView.layer.shadowColor = [UIColor grayColor].CGColor;
        _coverImageView.layer.shadowOffset = CGSizeMake(3, 3);
        _coverImageView.layer.shadowOpacity = 0.8;
        _coverImageView.layer.shadowRadius = 5;
        _coverImageView.clipsToBounds = NO;
        
        _coverImageEditImageView = [[UIImageView alloc] init];
        _coverImageEditImageView.image = [UIImage imageNamed:@"pen"];
        _coverImageEditImageView.alpha = 0.95;
        _coverImageEditImageView.backgroundColor = UIColor.blackColor;
        _coverImageEditImageView.contentMode = UIViewContentModeScaleAspectFit;
        _coverImageEditImageView.userInteractionEnabled = YES;
        _coverImageEditImageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        _songLabel = [[UILabel alloc] init];
        [_songLabel setFont:[UIFont systemFontOfSize:16]];
        [_songLabel setTextColor:UIColor.blackColor];
        _songLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        _artistAndAlbumLabel = [[UILabel alloc] init];
        [_artistAndAlbumLabel setFont:[UIFont systemFontOfSize:13]];
        [_artistAndAlbumLabel setTextColor:UIColor.darkGrayColor];
        _artistAndAlbumLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        _stackViewForSongAndArtistAlbumLabel = [[UIStackView alloc] init];
        [_stackViewForSongAndArtistAlbumLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        _stackViewForSongAndArtistAlbumLabel.axis = UILayoutConstraintAxisVertical;
        _stackViewForSongAndArtistAlbumLabel.distribution = UIStackViewDistributionEqualSpacing;
        _stackViewForSongAndArtistAlbumLabel.alignment = UIStackViewAlignmentLeading;
        _stackViewForSongAndArtistAlbumLabel.spacing = 2;
        
        _activityIndicatorForCoverImageView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicatorForCoverImageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self bringSubviewToFront:_coverImageEditImageView];
        
        [self addSubview:_coverImageView];
        [self addSubview:_coverImageEditImageView];
        [self addSubview:_stackViewForSongAndArtistAlbumLabel];
        [self addSubview:_activityIndicatorForCoverImageView];
        
        
        //CoverImageViewConstraints
        NSLayoutConstraint *coverImageViewTopConstraint =[NSLayoutConstraint
                                                          constraintWithItem:_coverImageView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                          toItem:self
                                                          attribute:NSLayoutAttributeTop
                                                          multiplier:1.0f
                                                          constant:12.f];
        NSLayoutConstraint *coverImageViewLeftConstraint =[NSLayoutConstraint
                                                           constraintWithItem:_coverImageView
                                                           attribute:NSLayoutAttributeLeft
                                                           relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                           attribute:NSLayoutAttributeLeft
                                                           multiplier:1.0f
                                                           constant:12.f];
        NSLayoutConstraint *coverImageViewHeightConstraint =[NSLayoutConstraint
                                                             constraintWithItem:_coverImageView
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1.0f
                                                             constant:68];
        NSLayoutConstraint *coverImageViewWidthConstraint =[NSLayoutConstraint
                                                            constraintWithItem:_coverImageView
                                                            attribute:NSLayoutAttributeWidth
                                                            relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0f
                                                            constant:68];
        
        //CoverImageEditViewConstraints
        NSLayoutConstraint *coverImageEditViewLeftConstraint =[NSLayoutConstraint
                                                               constraintWithItem:_coverImageEditImageView
                                                               attribute:NSLayoutAttributeLeft
                                                               relatedBy:NSLayoutRelationEqual
                                                               toItem:_coverImageView
                                                               attribute:NSLayoutAttributeLeft
                                                               multiplier:1.0f
                                                               constant:0.f];
        NSLayoutConstraint *coverImageEditViewCenterConstraint =[NSLayoutConstraint
                                                                 constraintWithItem:_coverImageEditImageView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual
                                                                 toItem:_coverImageView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                 multiplier:1.0f
                                                                 constant:0.f];
        NSLayoutConstraint *coverImageEditViewBottomConstraint =[NSLayoutConstraint
                                                                 constraintWithItem:_coverImageEditImageView
                                                                 attribute:NSLayoutAttributeBottom
                                                                 relatedBy:NSLayoutRelationEqual
                                                                 toItem:_coverImageView
                                                                 attribute:NSLayoutAttributeBottom
                                                                 multiplier:1.0f
                                                                 constant:0.f];
        NSLayoutConstraint *coverImageEditViewHeightConstraint =[NSLayoutConstraint
                                                                 constraintWithItem:_coverImageEditImageView
                                                                 attribute:NSLayoutAttributeHeight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1.0f
                                                                 constant:10];
        
        //StackViewForSongArtistAlbumConstraints
        NSLayoutConstraint *stackViewForSongArtistAlbumTopConstraint =[NSLayoutConstraint
                                                                       constraintWithItem:_stackViewForSongAndArtistAlbumLabel
                                                                       attribute:NSLayoutAttributeCenterY
                                                                       relatedBy:NSLayoutRelationEqual
                                                                       toItem:_coverImageView
                                                                       attribute:NSLayoutAttributeCenterY
                                                                       multiplier:1.0f
                                                                       constant:0.f];
        NSLayoutConstraint *stackViewForSongArtistAlbuLeftConstraint =[NSLayoutConstraint
                                                                       constraintWithItem:_stackViewForSongAndArtistAlbumLabel
                                                                       attribute:NSLayoutAttributeLeft
                                                                       relatedBy:NSLayoutRelationEqual
                                                                       toItem:_coverImageView
                                                                       attribute:NSLayoutAttributeRight
                                                                       multiplier:1.0f
                                                                       constant:24.f];
        
        
        //ActivityIndicatorViewforCoverImageView
        NSLayoutConstraint *activityIndicatorForCoverCenterXConstraint =[NSLayoutConstraint
                                                                         constraintWithItem:_activityIndicatorForCoverImageView
                                                                         attribute:NSLayoutAttributeCenterX
                                                                         relatedBy:NSLayoutRelationEqual
                                                                         toItem:_coverImageView
                                                                         attribute:NSLayoutAttributeCenterX
                                                                         multiplier:1.0f
                                                                         constant:0.f];
        NSLayoutConstraint *activityIndicatorForCoverCenterYConstraint =[NSLayoutConstraint
                                                                         constraintWithItem:_activityIndicatorForCoverImageView
                                                                         attribute:NSLayoutAttributeCenterY
                                                                         relatedBy:NSLayoutRelationEqual
                                                                         toItem:_coverImageView
                                                                         attribute:NSLayoutAttributeCenterY
                                                                         multiplier:1.0f
                                                                         constant:0.f];
        
        [self addConstraint:coverImageViewTopConstraint];
        [self addConstraint:coverImageViewLeftConstraint];
        [self addConstraint:coverImageViewHeightConstraint];
        [self addConstraint:coverImageViewWidthConstraint];
        
        [self addConstraint:coverImageEditViewLeftConstraint];
        [self addConstraint:coverImageEditViewCenterConstraint];
        [self addConstraint:coverImageEditViewHeightConstraint];
        [self addConstraint:coverImageEditViewBottomConstraint];
        
        [self addConstraint:stackViewForSongArtistAlbumTopConstraint];
        [self addConstraint:stackViewForSongArtistAlbuLeftConstraint];
        
        [self addConstraint:activityIndicatorForCoverCenterXConstraint];
        [self addConstraint:activityIndicatorForCoverCenterYConstraint];
    }
    return self;
}

-(void)setIsActivityIndicatorForCoverEnabled:(BOOL)isActivityIndicatorForCoverEnabled{
    if(isActivityIndicatorForCoverEnabled == YES){
        [_activityIndicatorForCoverImageView startAnimating];
    }
    else {
        [_activityIndicatorForCoverImageView stopAnimating];
    }
}

-(void)setMP3Tag:(MP3Tag*)mp3Tag{
    if([mp3Tag.album length] != 0 && [mp3Tag.artist length] != 0)
        self.artistAndAlbumLabel.text = [NSString stringWithFormat:@"%@ - %@", mp3Tag.artist, mp3Tag.album];
    else if([mp3Tag.artist length] != 0)
        self.artistAndAlbumLabel.text = mp3Tag.artist;
    else if([mp3Tag.album length] != 0)
        self.artistAndAlbumLabel.text = mp3Tag.album;
    
    [self.stackViewForSongAndArtistAlbumLabel addArrangedSubview:self.songLabel];
    [self.stackViewForSongAndArtistAlbumLabel addArrangedSubview:self.artistAndAlbumLabel];
    
    self.songLabel.text = mp3Tag.title;
    
    if(mp3Tag.coverWithMimeType.image != nil)
        self.coverImageView.image = mp3Tag.coverWithMimeType.image;
    else
        self.coverImageView.image = [UIImage imageNamed:@"mp3DefaultCover"];
}

@end
