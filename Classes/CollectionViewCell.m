//
//  CollectionViewCell.m
//  ID3 Editor
//
//  Created by Readdle on 28.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "CollectionViewCell.h"
#import "MP3Tag.h"
#import "CoverWithMimeType.h"

@interface CollectionViewCell()
@property UIImageView* coverImageView;
@property UILabel* titleLabel;
@property UILabel* artistLabel;
@property UILabel* albumLabel;
@property UILabel* genreLabel;
@end

@implementation CollectionViewCell

-(instancetype)init{
    self = [super init];
    //self.frame = CGRectMake(0, 0, 200, 50);
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    
    _coverImageView = [[UIImageView alloc] init];
    _coverImageView.image = [UIImage imageNamed:@"mp3DefaultCover"];
    _coverImageView.layer.cornerRadius = 15;
    _coverImageView.layer.masksToBounds = YES;
    _coverImageView.layer.borderWidth = 0;
    _coverImageView.contentMode = UIViewContentModeScaleAspectFill;
    _coverImageView.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.text = @"Unavailable🤷🏼‍♂️";
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.numberOfLines = 0;
    [_titleLabel setTextColor:UIColor.blackColor];
    _titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17.0];
    _titleLabel.adjustsFontSizeToFitWidth = TRUE;
    _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addSubview:_coverImageView];
    [self addSubview:_titleLabel];
    
    NSArray *coverHorizontalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[cover]-10-|" options:0 metrics:nil views:@{@"cover" : _coverImageView}];
    NSArray *coverVerticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[cover]-10-[title(20)]-10-|" options:0 metrics:nil views:@{@"cover" : _coverImageView, @"title" : _titleLabel}];
    NSArray *coverHorizontalConstraints2 = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[title]-10-|" options:0 metrics:nil views:@{@"title" : _titleLabel}];

    [self addConstraints:coverHorizontalConstraints];
    [self addConstraints:coverVerticalConstraints];
    [self addConstraints:coverHorizontalConstraints2];
    return self;
}

-(void)setMP3Tag:(MP3Tag*)tag{
    if(tag.coverWithMimeType.image != nil){
        _coverImageView.image = tag.coverWithMimeType.image;
    }
    else {
        _coverImageView.image = [UIImage imageNamed:@"mp3DefaultCover"];
    }
    //Check to NSString to nil
    if([tag.title length] != 0){
        _titleLabel.text = tag.title;
        NSLog(@"%@", _titleLabel.text);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

@end
