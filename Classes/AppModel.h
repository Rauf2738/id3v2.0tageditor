//
//  AppModel.h
//  ID3 Editor
//
//  Created by Readdle on 12.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MP3SelectListViewController.h"
@class NavigationController;


@interface AppModel : NSObject <MP3SelectListDelegate, TagEditorDataSource>

-(id)init:(id<MP3SelectListDataSource>)dataSource;

@property UINavigationController* navigationController;

@end
