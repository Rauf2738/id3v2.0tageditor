//
//  Image.m
//  ID3 Editor
//
//  Created by Readdle on 09.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "CoverWithMimeType.h"
#import "MP3Tag.h"

@implementation CoverWithMimeType

-(id)init{
    self = [super init];
    return self;
}

-(id)init:(UIImage*)image mimeType:(CoverMimeType)mimeType{
    self = [super init];
    self.image = image;
    self.mimeType = _mimeType;
    return self;
}

@end
