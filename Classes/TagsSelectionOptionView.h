//
//  ViewForTagSelectionViewController.h
//  ID3 Editor
//
//  Created by Readdle on 15.08.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MP3Tag;

@interface TagsSelectionOptionView : UIView

-(instancetype)initWithTag:(MP3Tag*)mp3Tag;

@property (nonatomic) BOOL isActivityIndicatorForCoverEnabled;

@end
