//
//  DataSourceFromFilePath.h
//  ID3 Editor
//
//  Created by Readdle on 01.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MP3SelectListViewController.h"

@interface DataSourceFromFilePath : NSObject <MP3SelectListDataSource>

-(id)init: (NSArray*)mp3FilePaths;

@end
