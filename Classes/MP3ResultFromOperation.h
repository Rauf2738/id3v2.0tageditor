//
//  MP3ResultFromOperation.h
//  ID3 Editor
//
//  Created by Readdle on 01.08.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GetMP3CoverOperation.h"

@interface MP3ResultFromOperation : NSObject

@property NSString* title;
@property NSString* artist;
@property NSString* album;
@property NSArray* coverIDs;
@property GetMP3CoverOperation* mp3CoverOperation;

@end
