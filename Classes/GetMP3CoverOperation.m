//
//  GetMP3CoverOperation.m
//  ID3 Editor
//
//  Created by Readdle on 26.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "GetMP3CoverOperation.h"

@interface GetMP3CoverOperation() <GetMP3CoverResultProtocol>

@property void (^completionBlock)(id<GetMP3CoverResultProtocol>);
@property NSString* coverID;

@property NSError *error;
@property BOOL isCancelled;
@property UIImage* cover;

@end

@implementation GetMP3CoverOperation

- (instancetype)initWithCoverID:(NSString *)coverId
{
    self = [super init];
    if (self) {
        self.coverID = coverId;
    }
    return self;
}

-(void)startWithCompletionBlock:(void (^)(id<GetMP3CoverResultProtocol>))completionBlock {
    self.completionBlock = completionBlock;
    [self getRequest:[NSString stringWithFormat: @"%s%@", "http://coverartarchive.org/release/", self.coverID]];
}

-(void)cancel{
    NSLog(@"Cancel");
}


-(void)getRequest:(NSString*)url{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        self.error = error;
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSLog(@"Request reply: %@", requestReply);
        
        NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
        
        if (statusCode == 200) {
            [self parseJSON:requestReply];
        }
        else{
            self.completionBlock(self);
        }
        
    }] resume];
}

-(void)parseJSON:(NSString*)json{
    
    NSData * jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSError *jsonError;
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
    NSMutableArray *jsonObjectArray = [jsonObject objectForKey:@"images"];
    
    NSString* imageUrl = [[jsonObjectArray objectAtIndex:0] valueForKeyPath:@"thumbnails.large"];
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: imageUrl]];
    if(imageData){
        self.cover = [UIImage imageWithData: imageData];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.completionBlock(self);
    });
}

@end
