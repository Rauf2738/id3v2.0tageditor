//
//  TagsToSelectViewController.h
//  ID3 Editor
//
//  Created by Readdle on 09.08.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MP3Tag;

@protocol TagSelectionDelegate

- (void)tagViewControllerNeedsLoadCoverForTag:(MP3Tag*)mp3Tag finishLoadingBLock:(void(^)(UIImage *cover))loadingBLock;
- (void)selectTagModel:(MP3Tag*)mp3Tag;

@end

@interface TagsSelectionViewController : UIViewController

@property (nonatomic) BOOL isCoverImageViewActivityIndicatorVisible;

-(instancetype)initWithArrayOfMP3Info:(NSArray<MP3Tag*>*)arrayOfMP3Tag;

@property id <TagSelectionDelegate>delegate;

@end
