//
//  Image.h
//  ID3 Editor
//
//  Created by Readdle on 09.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MP3Tag.h"


@interface CoverWithMimeType : NSObject

-(id)init:(UIImage*)image mimeType:(CoverMimeType)mimeType;

@property UIImage *image;
@property CoverMimeType *mimeType;

@end
