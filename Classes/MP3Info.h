//
//  MP3Info.h
//  ID3 Editor
//
//  Created by Readdle on 09.08.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MP3Tag.h"

@interface MP3Info : NSObject

@property MP3Tag *mp3Tags;
@property NSString *time;
@property NSString *kind;
@property NSString *size;
@property (nonatomic, strong) NSString *bitRate;

@end
