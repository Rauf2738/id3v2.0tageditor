//
//  MP3Tag.h
//  ID3 Editor
//
//  Created by Readdle on 29.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum{
    PNGMimeType, 
    JPGMimeType, 
    UnknownMimeType
} CoverMimeType;

@class CoverWithMimeType;

@interface MP3Tag : NSObject

@property NSString *title;
@property NSString *artist;
@property NSString *album;
@property NSString *genre;
@property NSString *track;
@property NSString *year;
@property NSString *coverID;
@property CoverWithMimeType *coverWithMimeType;

-(void)setCoverWithImage:(UIImage*)image mimeType:(CoverMimeType)mimeType;

@end
