//
//  GetMP3CoverOperation.h
//  ID3 Editor
//
//  Created by Readdle on 26.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol GetMP3CoverResultProtocol <NSObject>

- (NSError *)error;
- (BOOL)isCancelled;

- (UIImage *)cover;

@end

@interface GetMP3CoverOperation : NSObject 

- (instancetype)initWithCoverID:(NSString *)coverId;
- (void)startWithCompletionBlock:(void(^)(id<GetMP3CoverResultProtocol>))completionBlock;
- (void)cancel;

@end
