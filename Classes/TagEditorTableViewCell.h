//
//  TableViewCell.h
//  ID3 Editor
//
//  Created by Readdle on 05.08.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TagEditorTableViewCellProtocol <NSObject>

-(void)tagEditingCellDidChangeValue:(id)cell newValue:(NSString *)newValue;
-(void)tagEditingCellDidBeginEditing:(id)cell;

@end

@interface TagEditorTableViewCell : UITableViewCell

@property UILabel* titleToTextfield;
@property UITextField *textField;
@property id<TagEditorTableViewCellProtocol> delegate;

@end
