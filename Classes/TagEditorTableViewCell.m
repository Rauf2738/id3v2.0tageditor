//
//  TableViewCell.m
//  ID3 Editor
//
//  Created by Readdle on 05.08.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "TagEditorTableViewCell.h"
#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

@interface TagEditorTableViewCell() <UITextFieldDelegate>

@end

@implementation TagEditorTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.backgroundColor = UIColor.whiteColor;
        
        _titleToTextfield = [[UILabel alloc] init];
//        _titleToTextfield.translatesAutoresizingMaskIntoConstraints = NO;
        [_titleToTextfield setFont:[UIFont systemFontOfSize:14]];
        [_titleToTextfield setContentHuggingPriority:252 forAxis:0];
        _titleToTextfield.textAlignment = NSTextAlignmentRight;
        
        _textField = [[UITextField alloc] init];
        _textField.delegate = self;
        _textField.font = [UIFont boldSystemFontOfSize:15];
        _textField.userInteractionEnabled = YES;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//        _textField.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.contentView addSubview:_titleToTextfield];
        [self.contentView addSubview:_textField];
        
        _titleToTextfield.frame = CGRectMake(0, 0, 70, self.contentView.frame.size.height);
        _textField.frame = CGRectMake(80, 0, 280, self.contentView.frame.size.height);
        
        
        _titleToTextfield.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        _textField.autoresizingMask =  UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
//        NSLayoutConstraint *titlToTextfieldRightConstraint =[NSLayoutConstraint
//                                                            constraintWithItem:_titleToTextfield
//                                                            attribute:NSLayoutAttributeRight
//                                                            relatedBy:NSLayoutRelationEqual
//                                                            toItem:_textField
//                                                            attribute:NSLayoutAttributeLeft
//                                                            multiplier:1.0f
//                                                            constant:-30.f];
//        
//        NSLayoutConstraint *titleToTextfieldCenterConstraint =[NSLayoutConstraint
//                                                               constraintWithItem:_titleToTextfield
//                                                               attribute:NSLayoutAttributeCenterY
//                                                               relatedBy:NSLayoutRelationEqual
//                                                               toItem:self.contentView
//                                                               attribute:NSLayoutAttributeCenterY
//                                                               multiplier:1.0f
//                                                               constant:0.f];
//        
//        [_titleToTextfield setContentCompressionResistancePriority:751 forAxis:UILayoutConstraintAxisHorizontal];
//        
//        NSLayoutConstraint *textfieldTopConstraint =[NSLayoutConstraint
//                                                     constraintWithItem:_textField
//                                                     attribute:NSLayoutAttributeTop
//                                                     relatedBy:NSLayoutRelationEqual
//                                                     toItem:self.contentView
//                                                     attribute:NSLayoutAttributeTop
//                                                     multiplier:1.0f
//                                                     constant:0.f];
//        NSLayoutConstraint *textfieldBottomConstraint =[NSLayoutConstraint
//                                                        constraintWithItem:_textField
//                                                        attribute:NSLayoutAttributeBottom
//                                                        relatedBy:NSLayoutRelationEqual
//                                                        toItem:self.contentView
//                                                        attribute:NSLayoutAttributeBottom
//                                                        multiplier:1.0f
//                                                        constant:0.f];
//        NSLayoutConstraint *textfieldRightConstraint =[NSLayoutConstraint
//                                                       constraintWithItem:_textField
//                                                       attribute:NSLayoutAttributeRightMargin
//                                                       relatedBy:NSLayoutRelationEqual
//                                                       toItem:self.contentView
//                                                       attribute:NSLayoutAttributeRightMargin
//                                                       multiplier:1.0f
//                                                       constant:10.f];
//        NSLayoutConstraint *textfieldWidthConstraint =[NSLayoutConstraint
//                                                       constraintWithItem:_textField
//                                                       attribute:NSLayoutAttributeWidth
//                                                       relatedBy:NSLayoutRelationEqual
//                                                       toItem:nil
//                                                       attribute:NSLayoutAttributeNotAnAttribute
//                                                       multiplier:1.0f
//                                                       constant:self.contentView.bounds.size.width-_titleToTextfield.bounds.size.width-40];
//
//        
//        [self.contentView addConstraint:titlToTextfieldRightConstraint];
//        [self.contentView addConstraint:titleToTextfieldCenterConstraint];
//        
//        [self.contentView addConstraint:textfieldTopConstraint];
//        [self.contentView addConstraint:textfieldBottomConstraint];
//        [self.contentView addConstraint:textfieldRightConstraint];
//        [self.contentView addConstraint:textfieldWidthConstraint];
    }
    return self;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [_delegate tagEditingCellDidBeginEditing:self];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [_delegate tagEditingCellDidChangeValue:self newValue:self.textField.text];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
}

@end
