//  CollectionViewController.m
//  ID3 Editor
//
//  Created by Readdle on 28.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "MP3SelectListViewController.h"
#import "CollectionViewCell.h"
#import "TagManager.h"
#import "TagEditorViewController.h"
#import "MP3Tag.h"
#import "CoverWithMimeType.h"
#import "CollectionViewCell.h"

@interface MP3SelectListViewController ()

-(instancetype)init;
@property TagManager *tagManager;
@property BOOL selectItemSet;
@property UICollectionView * collectionView;

@end

@implementation MP3SelectListViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    _tagManager = [TagManager sharedInstance];
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    self.collectionView.backgroundColor = UIColor.whiteColor;
    [self.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.view addSubview:self.collectionView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_collectionView reloadData];
}

-(instancetype)init{
    self = [super init];
    self.title = @"MP3CollectionViewController";
    return self;
}

-(IBAction)selectItems:(id)sender{
    _selectItemSet = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_dataSource numberOfMP3FilesPaths];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake([UIApplication sharedApplication].keyWindow.frame.size.width/3.175, 150);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    MP3Tag *tag = [_delegate getTag:[_dataSource mp3FilePathAtIndex:indexPath.item]];
    [cell setMP3Tag:tag];
    return cell;
}

#pragma mark <UICollectionViewDelegate>

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    _selectedPaths = @[[_dataSource mp3FilePathAtIndex:indexPath.row]];
    [_delegate selectedPathsDidChange:self];
}

@end
