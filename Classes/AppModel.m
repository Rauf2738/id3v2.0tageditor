//
//  AppModel.m
//  ID3 Editor
//
//  Created by Readdle on 12.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "AppModel.h"
#import "MP3SelectListViewController.h"
#import "TagManager.h"
#import "CoverWithMimeType.h"
#import "TagsSelectionViewController.h"

// tags autofilling stuff
#import "MP3Info.h"
#import "MP3Tag.h"
#import "FingerprintModule.h"
#import "GetMP3TagsOperation.h"
#import "GetMP3CoverOperation.h"
#import "GetMp3InfoOperation.h"


@interface AppModel() <TagEditorDelegate, TagSelectionDelegate>

@property TagManager *tagManager;
@property NSArray <NSString *> *selectedPaths;
@property id <MP3SelectListDataSource> dataSource;
@property NSArray* coverArray;

@property TagEditorViewController * tagEditorViewController;
@property TagsSelectionViewController * tagSelectionViewController;

@end

@implementation AppModel

-(id)init:(id<MP3SelectListDataSource>)dataSource{
    self = [super init];
    _tagManager = [TagManager sharedInstance];
    self.dataSource = dataSource;
    
    MP3SelectListViewController *mp3SelectViewController = [[MP3SelectListViewController alloc] init];
    mp3SelectViewController.dataSource = self.dataSource;
    mp3SelectViewController.delegate = self;
    
    _navigationController = [[UINavigationController alloc] initWithRootViewController:mp3SelectViewController];
    
    return self;
}

-(void)tagViewControllerNeedsLoadCoverForTag:(MP3Tag *)mp3Tag finishLoadingBLock:(void (^)(UIImage *))loadingBLock{
    [_tagSelectionViewController setIsCoverImageViewActivityIndicatorVisible:YES];
    
    GetMP3CoverOperation *mp3CoverOperation = [[GetMP3CoverOperation alloc] initWithCoverID:mp3Tag.coverID];
    [mp3CoverOperation startWithCompletionBlock:^(id<GetMP3CoverResultProtocol> result) {
        [_tagSelectionViewController setIsCoverImageViewActivityIndicatorVisible:NO];
        if(result.error){
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Ooops"
                                                                          message:@"🤷🏼‍♂️"
                                                                   preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action)
                                       {
                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                       }];
            [alert addAction:okButton];
            [_navigationController presentViewController:alert animated:YES completion:nil];
        }
        else{
            if(result.cover){
                loadingBLock(result.cover);
            }
            else{
                loadingBLock([UIImage imageNamed:@"mp3DefaultCover"]);
            }
        }
    }];
}

-(void)selectTagModel:(MP3Tag *)mp3Tag{
    GetMp3InfoOperation *getMp3InfoOperation = [[GetMp3InfoOperation alloc] initWithMP3FileAtPath:_selectedPaths[0]];
    [getMp3InfoOperation startWithCompletionBlock:^(id<GetMP3InfoResultProtocol> result) {
        MP3Info *mp3Info = [[MP3Info alloc] init];
        
        if(mp3Tag != nil){
            
            mp3Info.mp3Tags = mp3Tag;
            
            mp3Info.time = result.time;
            mp3Info.size = result.size;
            mp3Info.bitRate = result.bitRate;
            mp3Info.kind = result.kind;
            
            [_navigationController popViewControllerAnimated:YES];
            [_tagEditorViewController setMp3Info:mp3Info];
            
        }
    }];
}

-(void)selectedPathsDidChange:(MP3SelectListViewController *)controller{
    _selectedPaths = controller.selectedPaths;
    
    _tagEditorViewController = [[TagEditorViewController alloc] init];
    _tagEditorViewController.dataSource = self;
    _tagEditorViewController.delegate = self;
    
    [_navigationController pushViewController:_tagEditorViewController animated:YES];
}

-(void)getMp3InfoWithFilePath:(NSString *)filePath completionBlock:(void (^)(MP3Info*))completionBlock{
    MP3Info *mp3Info = [[MP3Info alloc] init];
    GetMp3InfoOperation *getMp3InfoOperation = [[GetMp3InfoOperation alloc] initWithMP3FileAtPath:_selectedPaths[0]];
    [getMp3InfoOperation startWithCompletionBlock:^(id<GetMP3InfoResultProtocol> result) {
        mp3Info.time = result.time;
        mp3Info.size = result.size;
        mp3Info.bitRate = result.bitRate;
        mp3Info.kind = result.kind;
        
        mp3Info.mp3Tags = [_tagManager readTagsForMP3FileAtPath:_selectedPaths[0]];
    }];
    completionBlock(mp3Info);
}

#pragma mark AppModel Data Source
-(MP3Tag *)getTag:(NSString *)mp3Path{
    return [_tagManager readTagsForMP3FileAtPath:mp3Path];
}

-(NSString *)mp3SelectPath{
    return _selectedPaths[0];
}

#pragma mark Button Press
- (void)doneDidTapp:(MP3Tag *)mp3Tag filePath:(NSString *)filePath{
    [_tagManager setTags:mp3Tag forMP3FileAtPath:filePath];
    [_navigationController popViewControllerAnimated:YES];
}

- (void)findTagsDidTap{
    
    _tagEditorViewController.isFindButtonActivityIndicatorVisible = YES;
    
    GetMP3TagsOperation* getMp3TagsOperation = [[GetMP3TagsOperation alloc] initWithMP3FileAtPath:_selectedPaths[0]];
    [getMp3TagsOperation startWithCompletionBlock:^(id <GetMP3TagsResultProtocol> resultOfoperationModel) {
        if([resultOfoperationModel.mp3Tags count] > 1){
            _tagSelectionViewController = [[TagsSelectionViewController alloc] initWithArrayOfMP3Info:resultOfoperationModel.mp3Tags];
            _tagSelectionViewController.delegate = self;
            _tagEditorViewController.isFindButtonActivityIndicatorVisible = NO;
            [_navigationController pushViewController:_tagSelectionViewController animated:YES];
        }
        else if ([resultOfoperationModel.mp3Tags count] == 1)
        {
            GetMp3InfoOperation *getMp3InfoOperation = [[GetMp3InfoOperation alloc] initWithMP3FileAtPath:_selectedPaths[0]];
            [getMp3InfoOperation startWithCompletionBlock:^(id<GetMP3InfoResultProtocol> result) {
                MP3Info *mp3Info = [[MP3Info alloc] init];
                
                mp3Info.mp3Tags = [resultOfoperationModel.mp3Tags firstObject];
                
                mp3Info.time = result.time;
                mp3Info.size = result.size;
                mp3Info.bitRate = result.bitRate;
                mp3Info.kind = result.kind;
                
                [_tagEditorViewController setMp3Info:mp3Info];
                _tagEditorViewController.isCoverImageViewActivityIndicatorVisible = YES;
                _tagEditorViewController.isFindButtonActivityIndicatorVisible = NO;
                
                GetMP3CoverOperation *mp3CoverOperation = [[GetMP3CoverOperation alloc] initWithCoverID:[[resultOfoperationModel.mp3Tags firstObject] coverID]];
                [mp3CoverOperation startWithCompletionBlock:^(id<GetMP3CoverResultProtocol> result) {
                    mp3Info.mp3Tags.coverWithMimeType.image = result.cover;
                    [_tagEditorViewController setMp3Info:mp3Info];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _tagEditorViewController.isCoverImageViewActivityIndicatorVisible = NO;
                    });
                }];
            }];
        }
        else {
            _tagEditorViewController.isFindButtonActivityIndicatorVisible = NO;
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Ooops"
                                                                          message:@"🤷🏼‍♂️"
                                                                   preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action)
                                       {
                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                       }];
            [alert addAction:okButton];
            [_navigationController presentViewController:alert animated:YES completion:nil];
        }
    }];
}


@end
