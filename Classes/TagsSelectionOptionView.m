//
//  ViewForTagSelectionViewController.m
//  ID3 Editor
//
//  Created by Readdle on 15.08.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "TagsSelectionOptionView.h"
#import "MP3Tag.h"
#import "CoverWithMimeType.h"
#import "HeaderTagSelectionView.h"
#import "TagEditorTableViewCell.h"

@interface TagsSelectionOptionView()

@property HeaderTagSelectionView *header;
@property UIActivityIndicatorView *activityIndicatorForCover;

@end


@implementation TagsSelectionOptionView

-(void)setIsActivityIndicatorForCoverEnabled:(BOOL)isActivityIndicatorForCoverEnabled{
    if (isActivityIndicatorForCoverEnabled == YES){
        [_header setIsActivityIndicatorForCoverEnabled:YES];
    }
    else {
        [_header setIsActivityIndicatorForCoverEnabled:NO];
    }
}

-(instancetype)initWithTag:(MP3Tag *)mp3Tag{
    self = [super init];
    if(self){
        _header = [[HeaderTagSelectionView alloc] init];
        _header.coverImageEditImageView.hidden = YES;
        _header.translatesAutoresizingMaskIntoConstraints = NO;
        TagEditorTableViewCell *titleCell = [[TagEditorTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        titleCell.translatesAutoresizingMaskIntoConstraints = NO;
        TagEditorTableViewCell *artistCell = [[TagEditorTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];

        artistCell.translatesAutoresizingMaskIntoConstraints = NO;
        TagEditorTableViewCell *albumCell = [[TagEditorTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];

        albumCell.translatesAutoresizingMaskIntoConstraints = NO;
        
        [_header setMP3Tag:mp3Tag];
        titleCell.titleToTextfield.text = @"Song: ";
        artistCell.titleToTextfield.text = @"Artist: ";
        albumCell.titleToTextfield.text = @"Album: ";
        
        titleCell.textField.text = mp3Tag.title;
        artistCell.textField.text = mp3Tag.artist;
        albumCell.textField.text = mp3Tag.album;
        
        [self addSubview:_header];
        [self addSubview:titleCell];
        [self addSubview:artistCell];
        [self addSubview:albumCell];
//        [titleCell sizeToFit];
//        [artistCell sizeToFit];
//        [albumCell sizeToFit];
        
        NSDictionary *views = @{@"header":_header, @"titleCell":titleCell, @"artistCell":artistCell, @"albumCell":albumCell};
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[header]-10-[titleCell(44)]-10-[artistCell(44)]-10-[albumCell(44)]-|" options:NSLayoutFormatAlignAllCenterX metrics:nil views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[header]-|" options:0 metrics:nil views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[titleCell]-|" options:0 metrics:nil views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[artistCell]-|" options:0 metrics:nil views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[albumCell]-|" options:0 metrics:nil views:views]];
    }
    return self;
}

@end
