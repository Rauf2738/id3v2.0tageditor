//
//  MP3Tag.m
//  ID3 Editor
//
//  Created by Readdle on 29.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "MP3Tag.h"
#import "CoverWithMimeType.h"

@implementation MP3Tag

-(instancetype)init{
    self = [super init];
    if(self){
        self.coverWithMimeType = [CoverWithMimeType new];
    }
    return self;
}

-(void)setCoverWithImage:(UIImage *)image mimeType:(CoverMimeType)mimeType{
    _coverWithMimeType = [[CoverWithMimeType alloc] init:image mimeType:PNGMimeType];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ %@ %@", self.artist, self.title, self.album];
}

@end
