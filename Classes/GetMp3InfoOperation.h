//
//  GetMP3TagsOperation.h
//  ID3 Editor
//
//  Created by Readdle on 25.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GetMP3CoverOperation.h"

@protocol GetMP3InfoResultProtocol <NSObject>

- (NSError *)error;
- (BOOL)isCancelled;

- (NSString*) mp3Tags;
- (NSString*) time;
- (NSString*) kind;
- (NSString*) size;
- (NSString*) bitRate;

@end

@interface GetMp3InfoOperation : NSObject

- (instancetype)initWithMP3FileAtPath:(NSString*)filePath;
- (void)startWithCompletionBlock:(void (^)(id<GetMP3InfoResultProtocol>))completionBlock;
- (void)cancel;

@end
