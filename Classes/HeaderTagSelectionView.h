//
//  HeaderTagSelectionView.h
//  ID3 Editor
//
//  Created by Readdle on 19.08.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MP3Tag;
@interface HeaderTagSelectionView : UIView

@property UIImageView *coverImageView;
@property UIImageView *coverImageEditImageView;
@property UILabel *songLabel;
@property UILabel *artistAndAlbumLabel;
@property UIStackView* stackViewForSongAndArtistAlbumLabel;

- (instancetype)init;
- (instancetype)initWithFrame:(CGRect)frame NS_UNAVAILABLE;
- (void)setMP3Tag:(MP3Tag*)mp3Tag;

@property (nonatomic) BOOL isActivityIndicatorForCoverEnabled;

@end
