//
//  GetMp3InfoOperation.m
//  ID3 Editor
//
//  Created by Readdle on 09.08.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "GetMp3InfoOperation.h"
#import "MP3Info.h"
#import <AVFoundation/AVFoundation.h>

@interface GetMp3InfoOperation() <GetMP3InfoResultProtocol>
@property MP3Info* mp3Info;

@property NSString* mp3FilePath;
@property NSError* error;
@property BOOL *isCancelled;

@property NSString* mp3Tags;
@property NSString* time;
@property NSString* kind;
@property NSString* size;
@property NSString* bitRate;

@end

@implementation GetMp3InfoOperation


-(instancetype)initWithMP3FileAtPath:(NSString *)filePath{
    self = [super init];
    if(self){
        self.mp3FilePath = filePath;
    }
    return self;
}


-(void)startWithCompletionBlock:(void (^)(id<GetMP3InfoResultProtocol>))completionBlock{
    if(_mp3FilePath != nil){
        ///////////////Time//////////////
        NSURL *fileUrl = [[NSURL alloc] initFileURLWithPath:_mp3FilePath];
        AVURLAsset* audioAsset = [[AVURLAsset alloc] initWithURL:fileUrl options:nil];
        AVAssetTrack *track = [[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
        double duration = CMTimeGetSeconds(track.timeRange.duration);
        
        int seconds = [[NSNumber numberWithDouble:duration] intValue] % 60;
        int minutes = ([[NSNumber numberWithDouble:duration] intValue] / 60) % 60;
        
        _time = [NSString stringWithFormat:@"%2d:%02d", minutes, seconds];
        ///////////////Kind//////////////
        _kind = @"MP3";
        ///////////////Size//////////////
        CGFloat size = [[[NSFileManager defaultManager] attributesOfItemAtPath:_mp3FilePath error:nil] fileSize];
        _size = [NSByteCountFormatter stringFromByteCount:size countStyle:NSByteCountFormatterCountStyleFile];
        ///////////////Bit-Rate//////////////
        AVAssetReader* reader = [[AVAssetReader alloc] initWithAsset:audioAsset error:nil];
        double sampleRate = 0.0;
        double channels = 0.0;
        double depth = 0.0;
        NSDictionary *outputSettings = @{
                                         AVFormatIDKey: [NSNumber numberWithInt:kAudioFormatLinearPCM],
                                         AVLinearPCMIsBigEndianKey: [NSNumber numberWithInt:0],
                                         AVLinearPCMIsFloatKey: [NSNumber numberWithInt:0],
                                         AVLinearPCMBitDepthKey: [NSNumber numberWithInt:16],
                                         AVLinearPCMIsNonInterleaved: [NSNumber numberWithInt:0]
                                         };
        AVAssetReaderTrackOutput *trackOutput = [[AVAssetReaderTrackOutput alloc] initWithTrack:track outputSettings:outputSettings];
        [reader addOutput:trackOutput];
        [reader startReading];
        
        while(reader.status == AVAssetReaderStatusReading){
            CMSampleBufferRef sampleBufferRef = [trackOutput copyNextSampleBuffer];
            if(sampleBufferRef == NULL){
                [reader cancelReading];
            }
            else{
                CMFormatDescriptionRef formatDescription = CMSampleBufferGetFormatDescription(sampleBufferRef);
                const AudioStreamBasicDescription* const asbd = CMAudioFormatDescriptionGetStreamBasicDescription(formatDescription);
                
                if(asbd){
                    if(sampleRate == 0.0){
                        sampleRate = (asbd->mSampleRate)/1000;
                    }
                    if(channels == 0.0){
                        channels = asbd->mChannelsPerFrame;
                    }
                    if(depth == 0.0){
                        depth = asbd->mBitsPerChannel;
                    }
                    [reader cancelReading];
                }
            }
        }
        _bitRate = [NSString stringWithFormat:@"%d%@", (int)((sampleRate*channels*depth)/8), @" kbps"];
        
        completionBlock(self);
    }
}

-(void)cancel{
    NSLog(@"Cancel");
}

@end
