//
//  CollectionViewController.h//  ID3 Editor
//
//  Created by Readdle on 28.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <UIKit/UIKit.h>  
#import "TagEditorViewController.h"

@class MP3SelectListViewController;
@class MP3Tag;

//MP3DataSource Protocol
@protocol MP3SelectListDataSource
-(NSInteger)numberOfMP3FilesPaths;
-(NSString*)mp3FilePathAtIndex:(NSInteger)indexPath;
-(NSUInteger)indexOfMP3FilePath:(NSString*)filePath;
@end

//MP3ListViewControollerDelegate Protocol
@protocol MP3SelectListDelegate
- (void)selectedPathsDidChange:(MP3SelectListViewController*)controller;
- (MP3Tag*)getTag:(NSString*)mp3Path;
@end

@interface MP3SelectListViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property NSArray <NSString *> *selectedPaths;
@property id <MP3SelectListDataSource> dataSource;
@property id <MP3SelectListDelegate> delegate;

@end



