//
//  GetMP3TagsOperation.m
//  ID3 Editor
//
//  Created by Readdle on 25.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "GetMP3TagsOperation.h"
#import "MP3Tag.h"
#import "FingerprintModule.h"
#import "GetMP3CoverOperation.h"


@interface GetMP3TagsOperation () <GetMP3TagsResultProtocol>

@property void (^completionBlock)(id <GetMP3TagsResultProtocol> result);

@property NSMutableArray* coverIDs;
@property NSString* mp3Filepath;
@property double duration;


@property NSArray<MP3Tag *> *mp3Tags;
@property NSError *error;
@property BOOL isCancelled;

@property NSMutableArray<MP3Tag*> *responseObjects;

@end

@implementation GetMP3TagsOperation

-(instancetype)initWithMP3FileAtPath:(NSString *)filePath{
    self = [super init];
    if(self){
        self.mp3Filepath = filePath;
    }
    
    
    return self;
}

- (void)startWithCompletionBlock:(void (^)(id <GetMP3TagsResultProtocol> result))completionBlock{
    self.completionBlock = completionBlock;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSURL* songUrl = [[NSURL alloc] initFileURLWithPath:self.mp3Filepath];
        FingerprintModule *fp = [[FingerprintModule alloc] init];
        MP3FingerPrint *result = [fp generateFingerPrint:songUrl];
        
        [self getRequest:[NSString stringWithFormat: @"%s%d%s%@", "http://api.acoustid.org/v2/lookup?client=6LJNy9M40R&duration=", (int)result.duration, "&meta=recordings+releaseids+releasegroups&fingerprint=", result.fingerprint]];
    });
}

-(void)cancel{
    NSLog(@"cancel");
}

-(void)getRequest:(NSString*)url{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        self.error = error;
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSLog(@"Request reply: %@", requestReply);
        
        [self parseJSON:requestReply];
        
    }] resume];
}

-(void)parseJSON:(NSString*)json{
    
    NSData * jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSError *jsonError;
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
    _responseObjects = [[NSMutableArray alloc] init];
    
    NSArray* results = [jsonObject valueForKeyPath:@"results.recordings"];
    if (![[results firstObject] isKindOfClass:[NSNull class]] && [results count]>0){
        int count = 0;
        //Get NSArray from NSSingleObjectArray
        NSArray* tagVariation = [results objectAtIndex:0];
        
        for (int i = 0; i < [tagVariation count]; i++){
            NSArray* recordings = [tagVariation objectAtIndex:i];
            NSArray* releaseGroups = [recordings valueForKeyPath:@"releasegroups"];
            
            //Get each coverIds in releaseGroup
            for(int j = 0; j < [releaseGroups count]; j++){
                NSArray* releaseGroup = [releaseGroups objectAtIndex:j];
                
                if([[recordings valueForKeyPath:@"artists.name"] lastObject] == [[releaseGroup valueForKeyPath:@"artists.name"] lastObject]){
                    count++;
                    MP3Tag *mp3Tag = [[MP3Tag alloc] init];
                    
                    mp3Tag.title = [recordings valueForKeyPath:@"title"];
                    mp3Tag.artist = [[recordings valueForKeyPath:@"artists.name"] lastObject];
                    mp3Tag.genre = [[releaseGroup valueForKeyPath:@"secondarytypes"] firstObject];
                    mp3Tag.album = [releaseGroup valueForKeyPath:@"title"];
                    mp3Tag.coverID = [[releaseGroup valueForKeyPath:@"releases.id"] firstObject];
                    
                    [_responseObjects addObject:mp3Tag];
                }
            }
        }
        self.mp3Tags = [[NSArray alloc] initWithArray:_responseObjects];
    }
    dispatch_async(dispatch_get_main_queue(), ^(void){
        self.completionBlock(self);
    });
}

@end
