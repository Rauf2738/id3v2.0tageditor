//
//  CollectionViewCell.h
//  ID3 Editor
//
//  Created by Readdle on 28.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MP3Tag;
@interface CollectionViewCell : UICollectionViewCell

-(void)setMP3Tag:(MP3Tag*)tag;

@end
