//
//  TagManager.m
//  ID3 Editor
//
//  Created by Readdle on 29.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "TagManager.h"
#import "MP3Tag.h"
#import "id3v2lib.h"
#import "CoverWithMimeType.h"

#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

typedef NS_ENUM(NSUInteger, ID3TagEncodingType) {
    ID3TagWindowsCP1251Type = 0,
    ID3TagUTF16Type = 1,
    ID3TagUTF16BigEndianType = 2,
    ID3TagUTF8Type = 3,
};

@interface TagManager()
-(UIImage*)imageFilePathToSave:(UIImage*)image;
@end

@implementation TagManager

+ (instancetype)sharedInstance {
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

- (id)init{
    self = [super init];
    _cacheController = [NSCache new];
    return self;
}

- (void)setCacheCountLimit:(NSUInteger)countLimit{
    [_cacheController setCountLimit:countLimit];
}
- (void)setCacheTotalCost:(NSUInteger)costLimit{
    [_cacheController setTotalCostLimit:costLimit];
}


- (NSStringEncoding)encodingForTextContent:(ID3v2_frame_text_content)textContent
{
    NSStringEncoding result;
    switch (textContent.encoding) {
        case ID3TagWindowsCP1251Type:
            result = NSWindowsCP1251StringEncoding;
            NSLog(@"Windows1251");
            break;
        case ID3TagUTF16Type:
            result = NSUTF16StringEncoding;
            NSLog(@"UTF-16");
            break;
        case ID3TagUTF16BigEndianType:
            result = NSUTF16BigEndianStringEncoding;
            NSLog(@"UTF-16 BE");
            break;
        case ID3TagUTF8Type:
            result = NSUTF8StringEncoding;
            NSLog(@"UTF-8");
            break;
        default:
            result = NSUTF8StringEncoding;
            NSLog(@"Default");
            break;
    }
    return result;
}

- (MP3Tag *)readTagsForMP3FileAtPath:(NSString *)filePath
{
    MP3Tag *tags = [[MP3Tag alloc]init];
    if(![_cacheController objectForKey:filePath]){
        ID3v2_tag* tag = load_tag([filePath UTF8String]);
        NSLog(@"%@", filePath);
        
        ///////////////Title//////////////
        ID3v2_frame* title_frame = tag_get_title(tag); // Get the title frame
        ID3v2_frame_text_content* title_content = parse_text_frame_content(title_frame);
        if(title_content != nil){
            NSString *title_text = [NSString new];
            title_text = [[NSString alloc] initWithBytes:title_content->data length:title_content->size encoding: [self encodingForTextContent:*title_content]];
            NSLog(@"%c", title_content->encoding);
            tags.title = title_text;
        }
        
        ///////////////Artist//////////////
        ID3v2_frame* artist_frame = tag_get_artist(tag); // Get the full artist frame
        ID3v2_frame_text_content* artist_content = parse_text_frame_content(artist_frame);
        if(artist_content != nil){
            NSString *artist_text = [NSString new];
            artist_text = [[NSString alloc] initWithBytes:artist_content->data length:artist_content->size encoding:[self encodingForTextContent:*artist_content]];
            tags.artist = artist_text;
        }
        
        ///////////////Album//////////////
        ID3v2_frame* album_frame = tag_get_album(tag); // Get the album frame
        ID3v2_frame_text_content* album_content = parse_text_frame_content(album_frame);
        if(album_content != nil){
            NSString *album_text = [NSString new];
            album_text = [[NSString alloc] initWithBytes:album_content->data length:album_content->size encoding:[self encodingForTextContent:*album_content]];
            tags.album = album_text;
        }
        
        ///////////////Genre//////////////
        ID3v2_frame* genre_frame = tag_get_genre(tag); // Get the genre frame
        ID3v2_frame_text_content* genre_content = parse_text_frame_content(genre_frame);
        if(genre_content != nil){
            NSString *genre_text = [NSString new];
            genre_text = [[NSString alloc] initWithBytes:genre_content->data length:genre_content->size encoding:[self encodingForTextContent:*genre_content]];
            tags.genre = genre_text;
        }
        
        ///////////////Cover//////////////
        ID3v2_frame* cover_frame = tag_get_album_cover(tag); // Get the album frame
        ID3v2_frame_apic_content* cover_content = parse_apic_frame_content(cover_frame);
        if(cover_content != nil){
            NSData *image_data = [NSData dataWithBytes:cover_content->data length:cover_content->picture_size];
            tags.coverWithMimeType.image = [[UIImage new] initWithData:image_data];
        }
        else {
            tags.coverWithMimeType.image = [UIImage imageNamed:@"mp3DefaultCover"];
        }
        
        ///////////////Track//////////////
        ID3v2_frame* track_frame = tag_get_track(tag); // Get the track frame
        ID3v2_frame_text_content* track_content = parse_text_frame_content(track_frame);
        if(track_content != nil){
            NSString *track_text = [NSString new];
            track_text = [[NSString alloc] initWithBytes:track_content->data length:track_content->size encoding:[self encodingForTextContent:*track_content]];
            tags.track = track_text;
        }
        
        ///////////////Year//////////////
        ID3v2_frame* year_frame = tag_get_year(tag); // Get the year frame
        ID3v2_frame_text_content* year_content = parse_text_frame_content(year_frame);
        if(year_content != nil){
            NSString *year_text = [NSString new];
            year_text = [[NSString alloc] initWithBytes:year_content->data length:year_content->size encoding:[self encodingForTextContent:*year_content]];
            tags.year = year_text;
        }
        
        [_cacheController setObject:tags forKey:filePath];
    }
    else {
        tags = [_cacheController objectForKey:filePath];
    }
    return tags;
}

- (void)setTags:(MP3Tag *)tags forMP3FileAtPath:(NSString *)filePath{
    [_cacheController setObject:tags forKey:filePath];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        ID3v2_tag* tag = new_tag();
        
        // Set the new info
        if(tags.title != nil){
            tag_set_title((char*)[tags.title UTF8String], 03, tag);
        }
        if(tags.artist != nil){
            tag_set_artist((char*)[tags.artist UTF8String], 03, tag);
        }
        if(tags.album != nil){
            tag_set_album((char*)[tags.album UTF8String], 03, tag);
        }
        if(tags.genre != nil){
            tag_set_genre((char*)[tags.genre UTF8String], 03, tag);
        }
        if(tags.year != nil){
            tag_set_year((char*)[tags.year UTF8String], 03, tag);
        }
        if(tags.track != nil){
            tag_set_track((char*)[tags.track UTF8String], 03, tag);
        }
        
        // Get NSData from UIImage and set the ID3 album' cover
        NSData *album_cover_bytes = [NSData new];
        char *mimetype = NULL;
        tags.coverWithMimeType.image = [self imageFilePathToSave:tags.coverWithMimeType.image];
        //    if(tags.coverWithMimeType.mimeType == JPGMimeType){
        //        album_cover_bytes = UIImageJPEGRepresentation(tags.coverWithMimeType.image, 1);
        //        mimetype = "image/jpeg";
        //    }
        //    else if(tags.coverWithMimeType.mimeType == PNGMimeType) {
        //        album_cover_bytes = UIImagePNGRepresentation(tags.coverWithMimeType.image);
        //        mimetype = "image/png";
        //    }
        //    else {
        //        NSLog(@"Неподдерживаемый файл");
        //    }
        album_cover_bytes = UIImagePNGRepresentation(tags.coverWithMimeType.image);
        mimetype = "image/png";
        int picture_size = (int)album_cover_bytes.length;
        if(picture_size != 0){
            tag_set_album_cover_from_bytes((char *)[album_cover_bytes bytes], mimetype, picture_size, tag);
        }
        
        set_tag([filePath UTF8String], tag);
    });
}

- (void)removeTags:(NSString *)filePath{
    remove_tag([filePath UTF8String]);
    [_cacheController removeObjectForKey:filePath];
}

- (UIImage*)imageFilePathToSave:(UIImage*)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    CGRect rect = CGRectMake(0, 0, 512, 512);
    
    if(actualWidth >= 512 && actualHeight >= 512){
        if(actualWidth > actualHeight){
            float ratio=512/actualWidth;
            actualHeight = actualHeight*ratio;
            rect = CGRectMake(0.0, 0.0, 512, actualHeight);
        }
        else {
            float ratio=512/actualHeight;
            actualWidth = actualWidth*ratio;
            rect = CGRectMake(0.0, 0.0, actualWidth, 512);
        }
        UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1.0);
        [image drawInRect:rect];
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return img;
    }
    else{
        return image;
    }
}


@end
