//
//  TagsToSelectViewController.m
//  ID3 Editor
//
//  Created by Readdle on 09.08.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "TagsSelectionViewController.h"
#import "TagsSelectionOptionView.h"
#import "MP3Tag.h"
#import "CoverWithMimeType.h"
#import "HeaderTagSelectionView.h"
#import "TagEditorTableViewCell.h"

@interface TagsSelectionViewController () <UIScrollViewDelegate, UINavigationControllerDelegate>

@property UIScrollView *scrollViewForTag;
@property UIPageControl *pageControlForTag;
@property UIButton *selectButton;

@property NSArray<MP3Tag*> *arrayOfMP3Tag;
@property NSMutableArray<UIView *> *arrayOfUIView;

@end

@implementation TagsSelectionViewController

-(instancetype)initWithArrayOfMP3Info:(NSArray<MP3Tag*>*)arrayOfMP3Tag{
    self = [super init];
    if(self){
        NSLog(@"%@", arrayOfMP3Tag);
        _arrayOfMP3Tag = arrayOfMP3Tag;
    }
    return self;
}

-(void)loadView{
    [super loadView];
    self.view.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1.0];
    _arrayOfUIView = [[NSMutableArray alloc] init];
    _scrollViewForTag = [[UIScrollView alloc] init];
    _scrollViewForTag.pagingEnabled = YES;
    _scrollViewForTag.showsHorizontalScrollIndicator = NO;
    _scrollViewForTag.delegate = self;
    _scrollViewForTag.contentSize = CGSizeMake(self.view.frame.size.width * [_arrayOfMP3Tag count]-1, 400);
    _scrollViewForTag.translatesAutoresizingMaskIntoConstraints = NO;
    
    _pageControlForTag = [[UIPageControl alloc] init];
    _pageControlForTag.currentPageIndicatorTintColor = UIColor.darkGrayColor;
    _pageControlForTag.pageIndicatorTintColor = UIColor.lightGrayColor;
    _pageControlForTag.numberOfPages = [_arrayOfMP3Tag count];
    _pageControlForTag.currentPage = 0;
    _pageControlForTag.translatesAutoresizingMaskIntoConstraints = NO;
    
    _selectButton = [[UIButton alloc] init];
    [_selectButton setTitle:@"Select" forState:UIControlStateNormal];
    _selectButton.backgroundColor = UIColor.orangeColor;
    _selectButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_selectButton addTarget:self action:@selector(chooseTagModel:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_scrollViewForTag];
    [self.view addSubview:_pageControlForTag];
    [self.view addSubview:_selectButton];
    
    [self addViewToScrollView];
    
    //Костыль который просит картинку на первый UiView
    [_delegate tagViewControllerNeedsLoadCoverForTag:[_arrayOfMP3Tag objectAtIndex:0] finishLoadingBLock:^(UIImage *cover) {
        [_arrayOfMP3Tag objectAtIndex:0].coverWithMimeType.image  = cover;
        [self updateDataViewAtIndexPath:0];
    }];
    
    //ScrollViewConstraint
    NSLayoutConstraint *scrollViewTopConstraint =[NSLayoutConstraint
                                                  constraintWithItem:_scrollViewForTag
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                  toItem:self.view
                                                  attribute:NSLayoutAttributeTop
                                                  multiplier:1.0f
                                                  constant:0.f];
    NSLayoutConstraint *scrollViewLeftConstraint =[NSLayoutConstraint
                                                   constraintWithItem:_scrollViewForTag
                                                   attribute:NSLayoutAttributeLeft
                                                   relatedBy:NSLayoutRelationEqual
                                                   toItem:self.view
                                                   attribute:NSLayoutAttributeLeft
                                                   multiplier:1.0f
                                                   constant:0.f];
    NSLayoutConstraint *scrollViewRightConstraint =[NSLayoutConstraint
                                                    constraintWithItem:_scrollViewForTag
                                                    attribute:NSLayoutAttributeRight
                                                    relatedBy:NSLayoutRelationEqual
                                                    toItem:self.view
                                                    attribute:NSLayoutAttributeRight
                                                    multiplier:1.0f
                                                    constant:0.f];
    NSLayoutConstraint *scrollViewHeightConstraint =[NSLayoutConstraint
                                                     constraintWithItem:_scrollViewForTag
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                     toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                     multiplier:1.0f
                                                     constant:500.f];
    //PageControlConstraint
    NSLayoutConstraint *pageControlTopConstraint =[NSLayoutConstraint
                                                   constraintWithItem:_pageControlForTag
                                                   attribute:NSLayoutAttributeTop
                                                   relatedBy:NSLayoutRelationEqual
                                                   toItem:_scrollViewForTag
                                                   attribute:NSLayoutAttributeBottom
                                                   multiplier:1.0f
                                                   constant:0.f];
    NSLayoutConstraint *pageControlLeftConstraint =[NSLayoutConstraint
                                                    constraintWithItem:_pageControlForTag
                                                    attribute:NSLayoutAttributeLeft
                                                    relatedBy:NSLayoutRelationEqual
                                                    toItem:self.view
                                                    attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0f
                                                    constant:0.f];
    NSLayoutConstraint *pageControlRightConstraint =[NSLayoutConstraint
                                                     constraintWithItem:_pageControlForTag
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                     toItem:self.view
                                                     attribute:NSLayoutAttributeRight
                                                     multiplier:1.0f
                                                     constant:0.f];
    //ButtonConstraint
    NSLayoutConstraint *buttonTopConstraint =[NSLayoutConstraint
                                              constraintWithItem:_selectButton
                                              attribute:NSLayoutAttributeTop
                                              relatedBy:NSLayoutRelationEqual
                                              toItem:_pageControlForTag
                                              attribute:NSLayoutAttributeBottom
                                              multiplier:1.0f
                                              constant:0.f];
    NSLayoutConstraint *buttonLeftConstraint =[NSLayoutConstraint
                                               constraintWithItem:_selectButton
                                               attribute:NSLayoutAttributeLeft
                                               relatedBy:NSLayoutRelationEqual
                                               toItem:self.view
                                               attribute:NSLayoutAttributeLeft
                                               multiplier:1.0f
                                               constant:0.f];
    NSLayoutConstraint *buttonRightConstraint =[NSLayoutConstraint
                                                constraintWithItem:_selectButton
                                                attribute:NSLayoutAttributeRight
                                                relatedBy:NSLayoutRelationEqual
                                                toItem:self.view
                                                attribute:NSLayoutAttributeRight
                                                multiplier:1.0f
                                                constant:0.f];
    NSLayoutConstraint *buttonBottomConstraint =[NSLayoutConstraint
                                                 constraintWithItem:_selectButton
                                                 attribute:NSLayoutAttributeBottom
                                                 relatedBy:NSLayoutRelationEqual
                                                 toItem:self.view
                                                 attribute:NSLayoutAttributeBottom
                                                 multiplier:1.0f
                                                 constant:0.f];
    
    [self.view addConstraint:scrollViewTopConstraint];
    [self.view addConstraint:scrollViewLeftConstraint];
    [self.view addConstraint:scrollViewRightConstraint];
    [self.view addConstraint:scrollViewHeightConstraint];
    
    [self.view addConstraint:pageControlTopConstraint];
    [self.view addConstraint:pageControlLeftConstraint];
    [self.view addConstraint:pageControlRightConstraint];
    
    [self.view addConstraint:buttonTopConstraint];
    [self.view addConstraint:buttonRightConstraint];
    [self.view addConstraint:buttonLeftConstraint];
    [self.view addConstraint:buttonBottomConstraint];
}

-(void)addViewToScrollView{
    int xPosition = 0;
    for(int i = 0; i<[_arrayOfMP3Tag count]; i++){
        TagsSelectionOptionView *viewOfTagSelection = [[TagsSelectionOptionView alloc] initWithTag:[_arrayOfMP3Tag objectAtIndex:i]];
        
        //new CGRect with true origin
        CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, 300);
        frame.origin.x = xPosition;
        viewOfTagSelection.frame = frame;
        
        xPosition += self.view.frame.size.width;
        [_scrollViewForTag addSubview:viewOfTagSelection];
        [_arrayOfUIView addObject:viewOfTagSelection];
    }
}

-(void)updateDataViewAtIndexPath:(NSUInteger)indexPath{
    HeaderTagSelectionView *headerViewAtIndexPath = (HeaderTagSelectionView*)[[[_arrayOfUIView objectAtIndex:indexPath] subviews] objectAtIndex:0];
    headerViewAtIndexPath.coverImageView.image = [_arrayOfMP3Tag objectAtIndex:indexPath].coverWithMimeType.image;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    float fractionalPage = _scrollViewForTag.contentOffset.x/self.view.frame.size.width;
    NSUInteger indexPath = lround(fractionalPage);
    _pageControlForTag.currentPage = indexPath;
    if([_arrayOfMP3Tag objectAtIndex:indexPath].coverWithMimeType.image == nil){
        [_delegate tagViewControllerNeedsLoadCoverForTag:[_arrayOfMP3Tag objectAtIndex:indexPath] finishLoadingBLock:^(UIImage *cover) {
            [_arrayOfMP3Tag objectAtIndex:indexPath].coverWithMimeType.image = cover;
            [self updateDataViewAtIndexPath:indexPath];
        }];
    }
}

-(void)setIsCoverImageViewActivityIndicatorVisible:(BOOL)isCoverImageViewActivityIndicatorVisible{
    NSUInteger indexPath = _pageControlForTag.currentPage;
    
    if(isCoverImageViewActivityIndicatorVisible == YES){
        [(HeaderTagSelectionView*)[[[_arrayOfUIView objectAtIndex:indexPath] subviews] objectAtIndex:0] setIsActivityIndicatorForCoverEnabled:YES];
    }
    else{
        [(HeaderTagSelectionView*)[[[_arrayOfUIView objectAtIndex:indexPath] subviews] objectAtIndex:0] setIsActivityIndicatorForCoverEnabled:NO];
    }
}

-(void)chooseTagModel:(UIButton*)sender{
    NSUInteger indexPath = _pageControlForTag.currentPage;
    [_delegate selectTagModel:[_arrayOfMP3Tag objectAtIndex:indexPath]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
