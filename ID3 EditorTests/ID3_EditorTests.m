//
//  ID3_EditorTests.m
//  ID3 EditorTests
//
//  Created by Readdle on 6/26/17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TagManager.h"
#import "MP3Tag.h"
#import "CoverWithMimeType.h"
@interface ID3_EditorTests : XCTestCase

@end

@implementation ID3_EditorTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (NSString*)copyFileFromBundleToLibrary{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    
    NSArray *libraryPath = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *directoryPath = [libraryPath objectAtIndex:0];
    NSString *imagePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"25 Mezjdu razdrazjitelem i reakсiey.mp3"];
    NSString *mp3Path = [[NSString alloc] initWithString: directoryPath];
    mp3Path = [mp3Path stringByAppendingString: @"/25 Mezjdu razdrazjitelem i reakсiey.mp3"];
    if([fileManager fileExistsAtPath:mp3Path]){
        NSLog(@"Exist");
    }
    else{
        [fileManager copyItemAtPath:imagePathFromApp toPath:mp3Path error:nil];
    }
    return mp3Path;
}

//Write and Read Tags Test
- (void)testWriteAndReadTags {
    MP3Tag *mp3Tags = [[MP3Tag alloc] init];
    [mp3Tags setTitle:@"Title"];
    [mp3Tags setArtist:@"Artist"];
    [mp3Tags setAlbum:@"Album"];
    [mp3Tags setGenre:@"Genre"];
    [mp3Tags setCoverWithImage:[[UIImage alloc] initWithContentsOfFile:@"/Users/readdle/Desktop/ID3 Editor/ID3 EditorTests/testImage.png"] mimeType:PNGMimeType];
    TagManager *tagManager = [TagManager sharedInstance];
    [tagManager setTags:mp3Tags forMP3FileAtPath: [self copyFileFromBundleToLibrary]];
    
    MP3Tag *readMp3Tags = [[MP3Tag alloc] init];
    readMp3Tags = [tagManager readTagsForMP3FileAtPath: [self copyFileFromBundleToLibrary]];
    
    XCTAssertEqualObjects(mp3Tags.title, readMp3Tags.title);
    XCTAssertEqualObjects(mp3Tags.artist, readMp3Tags.artist);
    XCTAssertEqualObjects(mp3Tags.album, readMp3Tags.album);
    XCTAssertEqualObjects(mp3Tags.genre, readMp3Tags.genre);
    XCTAssertEqualObjects(UIImagePNGRepresentation(mp3Tags.coverWithMimeType.image),UIImagePNGRepresentation(readMp3Tags.coverWithMimeType.image));
}


//Set Image Test (64 * 64)
- (void)testImageSet64{
    MP3Tag *mp3Tags = [[MP3Tag alloc] init];
    [mp3Tags setCoverWithImage:[[UIImage alloc] initWithContentsOfFile:@"/Users/readdle/Desktop/ID3 Editor/ID3 EditorTests/202*202.png"] mimeType:PNGMimeType];
    
    TagManager *tagManager = [TagManager sharedInstance];
    [tagManager setTags:mp3Tags forMP3FileAtPath: [self copyFileFromBundleToLibrary]];
    
    MP3Tag *readMp3Tags = [[MP3Tag alloc] init];
    readMp3Tags = [tagManager readTagsForMP3FileAtPath: [self copyFileFromBundleToLibrary]];
    
    
    float currentSize = readMp3Tags.coverWithMimeType.image.size.height * readMp3Tags.coverWithMimeType.image.scale;
    float rightSize = 64;
    XCTAssertEqual(currentSize, rightSize);
}

//Set Image Test (256 * 256)
- (void)testImageSet256{
    MP3Tag *mp3Tags = [[MP3Tag alloc] init];
    [mp3Tags setCoverWithImage:[[UIImage alloc] initWithContentsOfFile:@"/Users/readdle/Desktop/ID3 Editor/ID3 EditorTests/400*400.png"] mimeType:PNGMimeType];
    
    TagManager *tagManager = [TagManager sharedInstance];
    [tagManager setTags:mp3Tags forMP3FileAtPath:[self copyFileFromBundleToLibrary]];
    
    MP3Tag *readMp3Tags = [[MP3Tag alloc] init];
    readMp3Tags = [tagManager readTagsForMP3FileAtPath: [self copyFileFromBundleToLibrary]];
    
    
    float currentSize = readMp3Tags.coverWithMimeType.image.size.height * readMp3Tags.coverWithMimeType.image.scale;
    float rightSize = 256;
    XCTAssertEqual(currentSize, rightSize);
}

- (void)testCacheWorking{
    NSCache *cacheController = [[NSCache alloc] init];
    [cacheController setTotalCostLimit:20];
    NSLog(@"1");
    [cacheController setObject:@"image1" forKey:@"image1" cost:10];
    [self getObjects:cacheController];
    NSLog(@"2");
    [cacheController setObject:@"image2" forKey:@"image2" cost:6];
    [self getObjects:cacheController];
    NSLog(@"3");
    [cacheController setObject:@"image3" forKey:@"image3" cost:4];
    [self getObjects:cacheController];
    NSLog(@"4");
    [cacheController setObject:@"image4" forKey:@"image4" cost:5];
    [self getObjects:cacheController];
}

- (void)getObjects:(NSCache*)cache{
    NSLog(@"%@", [cache objectForKey:@"image3"]);
    NSLog(@"%@", [cache objectForKey:@"image4"]);
    NSLog(@"%@", [cache objectForKey:@"image1"]);
    NSLog(@"%@", [cache objectForKey:@"image2"]);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
